<?php

/**
 * @file
 * Simple view template to display header_background view rows.
 *
 * @ingroup views_templates
 */
?>
<style type="text/css">
<?php foreach ($rows as $id => $row) { print $row; } ?>
</style>
