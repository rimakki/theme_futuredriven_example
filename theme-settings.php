<?php
/**
* @file
* Theme setting callbacks for the Future Driven theme.
*/
/**
* Implements hook_form_FORM_ID_alter().
*
* @param $form
*   The form.
* @param $form_state
*   The form state.
* @see http://www.metaltoad.com/blog/how-add-theme-settings-drupal-7
* @see https://www.drupal.org/node/177868
*/
function futuredriven_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['theme_settings']['futuredriven_realname_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Full name pattern'),
    '#default_value' => theme_get_setting('futuredriven_realname_pattern'),
    '#description' => t('Use this pattern to replace user name displays using tokens. <em>(Example: [user:field_user_full_name])</em>'),
    '#element_validate' => array('token_element_validate'),
    '#token_types' => array('user'),
    '#min_tokens' => 1,
    '#required' => FALSE,
    '#maxlength' => 256,
  );

  // Add the token tree UI.
  $form['theme_settings']['token_help'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('user'),
    '#global_types' => FALSE,
    '#dialog' => TRUE,
  );

}
